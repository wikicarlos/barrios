# En que Ayudo
## Description
Application for sharing in times of quarantine.
COVID-19 Response

## Database
postgis

## Backend
### URLS
* /admin
* /api/auth/login/
* /api/auth/create-account/
* /ayudoEndpoint
* /necesitoEndpoint

Agregar a los REQUESTS
Header Para Autenticar con el 
token que devuelve login  register.

Authorization: Token 3e541d265286065854a64fccd1d0d408bb606aef
(remplazar el token por el que devuelve el login o register,
y respetar el espacio despues de la palabra Token
)

Para acceder a los siguientes endpoints:
Todos los endpoints reciben parámetros POST
http://localhost:8000/accounts/citizen-validate-phone/
http://localhost:8000/accounts/citizen-register-phone/
http://localhost:8000/ayudoEndpoint/


Estan libres de AUTH los siguientes endpoints:
http://localhost:8000/accounts/citizen-register/
http://localhost:8000/accounts/citizen-login/

