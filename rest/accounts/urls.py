# -*- coding: utf-8 -*-
# Developer: Andres Anies <andres_anies@hotmail.com>

from django.conf.urls import url
from .views import LoginCitizenView, RegisterCitizenView
from . import views

urlpatterns = [
    url(r'^citizen-login/$', views.LoginCitizenView.as_view()),
    url(r'^citizen-register/$', views.RegisterCitizenView.as_view()),
    url(r'^citizen-register-phone/$', views.RegisterPhone.as_view()),
    url(r'^citizen-validate-phone/$', views.ValidatePhone.as_view()),
    url(r'^citizen-validate-dni/$', views.ValidateDni.as_view()),
]
