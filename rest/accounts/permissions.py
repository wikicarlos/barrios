# -*- coding: utf-8 -*-
"""
    File name: permissions
    Author: Edgar Arturo Haas Pacheco
    Date created: 18/02/2020
    Python Version: 3.7.3
"""
from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied
from django.apps import apps

