from django.contrib import admin
from .models import Contact, EmailVerification, PhoneVerification, Profile, Gestor, Caso, EventoCaso


admin.site.register(Contact)
admin.site.register(EmailVerification)
admin.site.register(PhoneVerification)
admin.site.register(Profile)

admin.site.register(Gestor)
admin.site.register(Caso)
admin.site.register(EventoCaso)


