# -*- coding: utf-8 -*-
"""
HTTP endpoints for sing-in and sing-up.
"""

from django.contrib.auth.models import User
from rest_framework import permissions, status
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.exceptions import ValidationError
from .serializers import CitizenApiRegisterSerializer, CitizenApiRegisterPhoneSerializer, CitizenApiValidatePhoneSerializer, CitizenApiValidateDniSerializer
import requests
from .models import Profile,PhoneVerification,Contact
from ..utils.notifications import send_sms


def authorize_registri_civil(primerNombre, segundoNombre, primerApellido, segundoApellido, fechadeNacimiento):
    # La funcion recibe toso los parametros necesarios para la validación
    # Si la validación, no fue correcta,devuelve False
    # Si la validación no fue ejecutada por problemas del servicio, devuelve None.
    payload = {
             "ENQUEAYUDOORG_APIKEY": "bd6cba9bd1a6a3ca36bc750e6a505ab820a0b73ef9a3e283e403acd1c7b1c3d3",
             "primerNombre": primerNombre.strip().upper().replace(' ', ''),
             "segundoNombre": segundoNombre.strip().upper().replace(' ', ''),
             "primerApellido": primerApellido.strip().upper().replace(' ', ''),
             "segundoAPELLIDO": segundoApellido.strip().upper().replace(' ', ''),
             "fechaNacimiento": fechadeNacimiento.strip().upper().replace('-', '/')}
    print(payload)
    response = requests.post('https://apiexterno.enqueayudo.org/api/auth/check-registro-civil/', data=payload)
    if response.status_code == requests.codes.ok or response.status_code == requests.codes.created:
        response_data = response.json()
        if 'resultRegistro' in response_data:
            return response_data['resultRegistro']
    else:
        # todo: raise log
        return None




class ValidatePhone(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = CitizenApiValidatePhoneSerializer

    def post(self, request, *args, **kwargs):
        code = request.data['code']
        contact = request.user.contact
        # contact.save()

        if contact.phone_verification.code == code:
            contact.phone_verification.verified = True
            contact.phone_verification.save()
            return Response({
                'result': '',
            }, status=200)
        else:
            return Response({
                'result': '',
            }, status=400)

class RegisterPhone(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = CitizenApiRegisterPhoneSerializer

    def post(self, request, *args, **kwargs):
        contact = request.user.contact
        contact.phone = request.data['phone']

        contact.save()
        if not hasattr(contact, 'phone_verification'):
            contact.phone_verification = PhoneVerification(
                contact=contact,
                code=PhoneVerification.generate_code(),
                tries=0
            )
            contact.phone_verification.save()
            contact.save()
        message = '{0} es tu codigo de verificacion enQueAyudo.ORG'.format(
            contact.phone_verification.code)
        send_sms(contact.phone, message)
        contact.phone_verification.tries = contact.phone_verification.tries + 1
        contact.save()

        return Response({
            'result': '',
        }, status=200)



class RegisterCitizenView(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = CitizenApiRegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        dni = authorize_registri_civil(validated_data.get('first_name'), validated_data.get('second_name'),
                                       validated_data.get('first_lastname'), validated_data.get('second_lastname'),
                                       validated_data.get('birth_date'))
        print(dni)
        if dni is not None:
            if dni is not False:
                pass
            else:
                msg = 'DNI not valid'
                raise ValidationError(msg)
        else:
            msg = 'Error from Registro Civil'
            raise ValidationError(msg)




        password = validated_data.get('birth_date')

        user_exists = User.objects.filter(username = dni['resultadoCedula'].lower().strip())

        if user_exists.count():
            return Response({
                'token': Token.objects.get_or_create(user=user_exists[0])[0].key,
                'first_name': user_exists[0].first_name,
            }, status=200)


        user = User()
        user.username =  dni['resultadoCedula'].lower().strip()
        user.first_name = validated_data.get('first_name')
        user.las_name = dni['resultadoRazonSocial']
        user.set_password(password)
        user.save()
        contact = Contact()
        contact.user = user
        contact.save()

        profile = Profile()
        profile.user = user

        try:
            profile.condition = dni['condicionCedulado']
        except:
            pass

        profile.first_name = validated_data.get('first_name')
        profile.second_name = validated_data.get('second_name')
        profile.first_lastname = validated_data.get('first_lastname')
        profile.second_lastname = validated_data.get('second_lastname')
        profile.complete_name = dni['resultadoRazonSocial']
        profile.three_last_dni = dni['resultadoCedula'][-3:]
        profile.save()

        return Response({
            'token': Token.objects.get_or_create(user=user)[0].key,
            'first_name': user.first_name,
        }, status=200)


class LoginCitizenView(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = AuthTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']

        return Response({
            'token': Token.objects.get_or_create(user=user)[0].key,
            'first_name': user.first_name,
        }, status=200)



class ValidateDni(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = CitizenApiValidateDniSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        code = validated_data['code']

        profile = request.user.profile


        if not profile.user_validated_dni:
            three_last_dni_digits = profile.three_last_dni
            if code == three_last_dni_digits:
                profile.user_validated_dni = True
                profile.save()
                return Response({
                    'result': 'DNI CONFIRMED',
                }, status=200)
            else:
                profile.user_validated_dni = False
                profile.save()
                return Response({
                    'result': 'DNI NOT CONFIRMED',
                }, status=400)

        else:
            return Response({
                'result': 'DNI ALREADY CONFIRMED',
            }, status=200)