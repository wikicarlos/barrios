from rest_framework import serializers

class CitizenApiSerializer(serializers.Serializer):
    first_name = serializers.CharField()
    second_name = serializers.CharField()

    class Meta:
        pass



class CitizenApiValidateDniSerializer(serializers.Serializer):
    code = serializers.CharField()

    class Meta:
        pass


class CitizenApiValidatePhoneSerializer(serializers.Serializer):
    code = serializers.CharField()

    class Meta:
        pass

class CitizenApiRegisterPhoneSerializer(serializers.Serializer):
    phone = serializers.CharField()

    class Meta:
        pass


class CitizenApiRegisterSerializer(serializers.Serializer):
    first_name = serializers.CharField()
    second_name = serializers.CharField(allow_blank=True)
    first_lastname = serializers.CharField()
    second_lastname = serializers.CharField(allow_blank=True)
    birth_date = serializers.CharField()
    dni = serializers.CharField(required=False)

    class Meta:
        pass

