import random
import string
from django.db import models
from django.contrib.auth.models import User
from ..necesito.models import Necesito


class Gestor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='gestor')
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(max_length=20,null=True, blank=True)

class Caso(models.Model):
    necesito = models.OneToOneField(Necesito, on_delete=models.CASCADE)
    gestor = models.ForeignKey(Gestor, on_delete=models.CASCADE)

class EventoCaso(models.Model):
    CASE_TYPES_CHOICES = [
        ('IN', 'Ingresado'),
        ('AS', 'Asignado'),
        ('EN', 'En Revisión'),
        ('SE', 'Seguimiento'),
    ]
    tipo = models.CharField(max_length=5, choices=CASE_TYPES_CHOICES)
    caso = models.ForeignKey(Caso, on_delete=models.CASCADE,related_name='evento_caso')
    comment = models.TextField(blank=True)

class Contact(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='contact')
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(max_length=20,null=True, blank=True)

    @property
    def email_verified(self):
        if hasattr(self, 'email_verification'):
            return self.email_verification.verified

    @property
    def phone_verified(self):
        if hasattr(self, 'phone_verification'):
            return self.phone_verification.verified

    def __str__(self):
        return str(self.user)



class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=30, blank=True)
    second_name = models.CharField(max_length=30, blank=True)
    first_lastname = models.CharField(max_length=30, blank=True)
    second_lastname = models.CharField(max_length=30, blank=True)
    condition = models.CharField(max_length=30, blank=True)

    complete_name = models.CharField(max_length=100, blank=True)
    three_last_dni = models.CharField(max_length=100, blank=True)

    user_validated_dni = models.NullBooleanField()



class EmailVerification(models.Model):

    MAX_TRIES = 5

    contact = models.OneToOneField(
        Contact, on_delete=models.CASCADE, related_name='email_verification')
    slug = models.CharField(max_length=64, unique=True)
    short_slug = models.CharField(max_length=6, unique=False)

    created_timestamp = models.DateTimeField(auto_now_add=True)
    verified = models.BooleanField(default=False)
    tries = models.IntegerField(default=1)

    @staticmethod
    def generate_slug():
        return ''.join(random.choices(string.ascii_letters + string.digits, k=64))

    @staticmethod
    def generate_short_slug():
        return ''.join(random.choices(string.digits, k=6))

    def __str__(self):
        return self.slug


class PhoneVerification(models.Model):

    MAX_TRIES = 3

    contact = models.OneToOneField(
        Contact, on_delete=models.CASCADE, related_name='phone_verification')
    code = models.CharField(max_length=6, unique=True)
    created_timestamp = models.DateTimeField(auto_now_add=True)
    verified = models.BooleanField(default=False)
    tries = models.IntegerField(default=1)

    @staticmethod
    def generate_code():
        return ''.join(random.choices(string.digits, k=6))

    def __str__(self):
        return self.code


class UIState(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    state = models.TextField()
