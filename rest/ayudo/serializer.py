from rest_framework import fields, serializers
from rest.ayudo.models import Ayudo



class AyudoSerializer(serializers.ModelSerializer):
    tipos_ayuda = fields.MultipleChoiceField(choices=Ayudo.AYUDO_CHOICES)

    class Meta:
        model = Ayudo
        fields = ('tipos_ayuda', 'ubicacion_ayuda', 'detalles')
