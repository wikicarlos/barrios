from rest_framework.views import APIView
from rest_framework.response import Response
from rest.ayudo.serializer import AyudoSerializer
from rest.ayudo.models import Ayudo
from rest_framework import permissions, viewsets
from django.shortcuts import get_object_or_404

class AyudoViewSet(viewsets.ModelViewSet):
    queryset = Ayudo.objects.all()
    serializer_class = AyudoSerializer
    permission_classes = [permissions.IsAuthenticated]

