from django.urls import include, path
from rest_framework import routers
from rest.ayudo import views


router = routers.DefaultRouter()
router.register(r'ayudoEndpoint', views.AyudoViewSet)

urlpatterns = [
    path(r'', include(router.urls), name='base')
]
