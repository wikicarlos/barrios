from django.db import models
from multiselectfield import MultiSelectField


class Ayudo(models.Model):
    AYUDO_CHOICES = (
        ('CYA', 'Comida - Agua'),
        ('MED', 'Medicamentos'),
        ('MAS', 'Mascarillas - Guantes - Alcohol'),
        ('SAL', 'Asistencia médica o emocional'),
        ('ANI', 'Asistencia para animales de compañia'),
        ('MAN', 'Mandados o Entregas'),
    )

    ubicacion_ayuda = models.CharField(max_length=256)
    nombre = models.CharField(max_length=256,blank=True)
    telefono = models.CharField(max_length=15,blank=True)
    tipos_ayuda = MultiSelectField(choices=AYUDO_CHOICES, null=True)
    detalles = models.TextField(blank=True)