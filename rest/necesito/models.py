from django.db import models
import uuid
import random
import string

class Necesito(models.Model):
    tipo_necesidad = models.CharField(max_length=3)
    ubicacion_necesidad = models.CharField(max_length=256)
    detalles = models.CharField(max_length=256)


    documento = models.CharField(max_length=50,blank=True)
    nombre = models.CharField(max_length=50,blank=True)
    telefono = models.CharField(max_length=15,blank=True)

    code = models.CharField(max_length=15,blank=True,default='')
    phone_confirmed = models.BooleanField(default=False, editable=False)

    confirmation_id = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)


    @staticmethod
    def generate_code():
        return ''.join(random.choices(string.digits, k=6))

