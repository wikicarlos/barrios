from rest_framework.views import APIView
from rest_framework.response import Response
from rest.necesito.serializer import NecesitoSerializer, VerifyNecesitoSerializer
from rest.necesito.models import Necesito
from rest_framework import permissions, viewsets
from django.shortcuts import get_object_or_404
from rest_framework import generics
from ..utils.notifications import send_sms

class NecesitoViewSet(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = NecesitoSerializer
    queryset = Necesito.objects.all()

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        necesito = Necesito()
        necesito.tipo_necesidad = validated_data['tipo_necesidad']
        necesito.ubicacion_necesidad = validated_data['ubicacion_necesidad']
        necesito.detalles = validated_data['detalles']
        necesito.nombre = validated_data['nombre']
        necesito.telefono = validated_data['telefono']
        necesito.code = Necesito.generate_code()
        necesito.save()
        message = '{0} es tu codigo de verificacion enQueAyudo.ORG'.format(
            necesito.code)
        send_sms(necesito.telefono, message)

        return Response({
            'verify': necesito.confirmation_id,
        }, status=200)


class VerifyNecesitoViewSet(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = VerifyNecesitoSerializer
    # queryset = Necesito.objects.all()

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        necesito = Necesito.objects.get(confirmation_id = validated_data['verify'], code = validated_data['code'] )
        necesito.phone_confirmed = True
        necesito.save()

        return Response({
            'verify': necesito.confirmation_id,
        }, status=200)