from rest_framework import serializers
from rest.necesito.models import Necesito


class VerifyNecesitoSerializer(serializers.Serializer):
    code = serializers.CharField()
    verify = serializers.CharField()

    class Meta:

        pass


class NecesitoSerializer(serializers.Serializer):
    tipo_necesidad = serializers.CharField()
    ubicacion_necesidad = serializers.CharField()
    detalles = serializers.CharField()
    nombre = serializers.CharField()
    telefono = serializers.CharField()

    class Meta:
        model = Necesito
        fields = ('tipo_necesidad', 'ubicacion_necesidad', 'detalles', 'nombre', 'telefono')