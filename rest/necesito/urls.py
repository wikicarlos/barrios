from django.urls import include, path
from rest_framework import routers
from rest.necesito import views
from django.conf.urls import url


urlpatterns = [
    url(r'^necesitoEndpoint/$', views.NecesitoViewSet.as_view()),
    url(r'^verifyNecesitoEndpoint/$', views.VerifyNecesitoViewSet.as_view()),
]
