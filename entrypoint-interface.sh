#!/bin/bash
echo "Hello from entrypoint-interface.sh"
cd /usr/src/app
python manage.py migrate
#exec daphne -b 0.0.0.0 -p 8000 barrio.asgi:application
exec python manage.py runserver 0.0.0.0:8000